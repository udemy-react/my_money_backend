const port = 3003

const express = require('express')
const server = express()

// Retorno em JSON
const bodyParser = require('body-parser')
server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())

// Liberação CORS
const allowCors = require('./cors')
server.use(allowCors)

// Paginação
const queryParser = require('express-query-int')
server.use(queryParser())

// Levanta o servidor de banco
server.listen(port, function () {
    console.log("BACKEND esta rodando na porta: " + port)
})

module.exports = server