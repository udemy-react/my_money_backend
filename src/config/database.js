const mongoose = require('mongoose')
mongoose.Promise = global.Promise

module.exports = mongoose.connect('mongodb://localhost/mymoney', {useMongoClient: true})

mongoose.Error.messages.general.required = 'O atributo {PATH} é obrigatorio' 
mongoose.Error.messages.Number.min = 'O valor {VALUE} é menor que {MIN}' 
mongoose.Error.messages.Number.max = 'O valor {VALUE} é maior que {MAX}' 
mongoose.Error.messages.String.enum = 'O valor {VALUE} é inválido: {PATH}' 